This module provides a Drupal Commerce payment method for the SecurePay Direct
Post service.

Installation
------------

Install the module in /sites/all/modules or wherever you have your commerce
module.

When you enable the module it must be configured in
/admin/commerce/config/payment-methods before you can use it. Edit the 'Credit
Card (SecurePay Direct Post)' payment method rule, then select edit in the
'Enable payment method: Credit Card (SecurePay Direct Post)' action.

Set test or live mode, default is test.

Set your merchant id provided by SecurePay in the format DEM0010.


Use
---

The new payment method will now be available, you can select
'pay by credit card' and an additional pane is provided for the redirect to
SecurePay. The module is set to only supply the order total to the SecurePay
for authorisation.
